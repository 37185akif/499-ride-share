<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('getStartingPoints.json', 'JsonController@getStartingPoints');
Route::post('createUser.json', 'JsonController@registration');
Route::post('login.json', 'JsonController@login');
Route::post('createRequest.json', 'JsonController@registerRequest');
Route::post('searchRide.json', 'JsonController@searchRidePartner');
Route::post('deleteRide.json', 'JsonController@deleteRide');
