<?php

use App\StartingPoints;
use Illuminate\Database\Seeder;

class StartingPointTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('start_points')->truncate();
        $inputs = [
            ['place_name' => 'NSU 8 No. Gate', 'longitude' => '90.4260567', 'latitude' => '23.8155671'],
            ['place_name' => 'NSU 2 NO. Gate', 'longitude' => '90.4250555', 'latitude' => '23.8147984'],
        ];

        foreach ($inputs as $input) {
            StartingPoints::create($input);
        }
    }
}
