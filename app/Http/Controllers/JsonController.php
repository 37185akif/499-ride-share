<?php

namespace App\Http\Controllers;

use App\StartingPoints;
use App\User;
use App\UserRequests;
use App\UserRide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class JsonController extends Controller
{
    public function getStartingPoints()
    {
        $starting_points = StartingPoints::get();
        return Response::json($starting_points);
    }


    //function to register a new user
    public function registration()
    {

        if ($this->emailExist($_POST["email"])) {
            return Response::json(array("result" => "Email already exists"), 201);
        } else if ($this->phoneExist($_POST["phone"])) {
            return Response::json(array("result" => "Phone number already exists"), 201);
        } else if ($this->studentExist($_POST["student_id"])) {
            return Response::json(array("result" => "This student id already exists"), 201);
        } else {
            $name = $_POST["name"];
            $email = $_POST["email"];
            $phone = $_POST["phone"];
            $student_id = $_POST["student_id"];
            $password = $_POST["password"];
            $address = $_POST["address"];

            //save data in user table
            $user = new User([
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'student_id' => $student_id,
                'password' => $password,
                'address' => $address]);
            $user->save();

            return Response::json($user, 200);
        }
    }


    //function to register a new user
    public function login()
    {
        //get user data from login creds
        $phone = $_POST["phone"];
        $password = $_POST["password"];

        $user = User::where('phone', $phone)->
        where('password', $password)
            ->first();
        if (!$user) {
            return Response::json(array("result" => "Wrong phone number or password"), 201);
        } else {
            return Response::json($user, 200);
        }


    }


    //function to create a ride request
    public function registerRequest()
    {
        //get user data from login creds
        $distance_range = 2;
        $max_user = 4;
        $user_id = $_POST["user_id"];
        $start_point_id = $_POST["start_point_id"];
        $destination = $_POST["destination"];
        $longitude = $_POST["longitude"];
        $latitude = $_POST["latitude"];
        $user_count = $_POST["user_count"];

        $start_point = StartingPoints::where('id', $start_point_id)->first();
        $destination_distance = $this->calculateDistance($latitude, $longitude, $start_point->latitude, $start_point->longitude, 'K');

        if ($this->requestExist($user_id)) {
            return Response::json(array("result" => "There is a pending request. You want to continue with that request or cancel ?"), 201);
        } else {
            //new request generated
            $new_request = new UserRequests([
                'user_id' => $user_id,
                'start_point_id' => $start_point_id,
                'destination' => $destination,
                'longitude' => $longitude,
                'latitude' => $latitude,
                'user_count' => $user_count,
                'distance' => $destination_distance,
                'ride_status' => 0,

            ]);
            $new_request->save();

            //calculation for matching user
            $temp_matching = [];
            $temp_user_count = $new_request->user_count;
            $count = 0;
            $user_requests = UserRequests::where('ride_status', 0)
                ->where('start_point_id', $start_point_id)
                ->where('id', '!=', $new_request->id)
                ->get();


            foreach ($user_requests as $user_request) {
                if ($temp_user_count < $max_user) {
                    $distance = $this->calculateDistance($new_request->latitude, $new_request->longitude, $user_request->latitude, $user_request->longitude, 'K');
                    if ($distance < $distance_range) {
                        if (($user_request->user_count + $temp_user_count) <= $max_user) {
                            $temp_matching[$count] = $user_request;
                            $count++;
                            $temp_user_count = $temp_user_count + $user_request->user_count;
                        }
                    }
                }
            }

            //after user matched calculation
            if ($temp_user_count == $max_user) {
                $temp_matching[$count + 1] = $new_request;
                $max_distance = 0;
                $fare_per_km = 10;
                $ride_code = $this->createRideCode();
                foreach ($temp_matching as $item) {
                    if ($max_distance < $item->distance) {
                        $max_distance = $item->distance;
                    }
                }
                $total_fare = $max_distance * $fare_per_km;
                $per_person_fare = $total_fare / $temp_user_count;
                foreach ($temp_matching as $item) {
                    $matched_ride = new UserRide([
                        'request_id' => $item->id,
                        'ride_fare' => $per_person_fare * $item->user_count,
                        'share_code' => $ride_code,
                    ]);
                    $matched_ride->save();

                    UserRequests::where('id', $item->id)
                        ->update(['ride_status' => 1]);
                }
            }

            return Response::json($new_request, 200);
        }

    }


    //search for your ride partner
    public function searchRidePartner()
    {
        $request_id = $_POST["request_id"];

        $user_ride = UserRide::where('request_id', $request_id)->with('request.user','request.startPoint')->first();
        if($user_ride){
            $all_users = UserRide::where('share_code', $user_ride->share_code)
                ->where('request_id', '!=', $user_ride->request_id)->with('request.user')->get();
            return Response::json(array("other_user" => $all_users,"own" => $user_ride), 200);
        }else{
            return Response::json(array("result" => "Please wait we're trying search your ride partners."), 201);
        }



    }

    //delete ride request
    public function deleteRide()
    {
        $request_id = $_POST["request_id"];

        UserRequests::where('id', $request_id)->delete();
        UserRide::where('request_id', $request_id)->delete();


        return Response::json(array("result" => "Your previous request has been cancelled. You can request for new ride now."), 200);

    }

    //random ride code to meet user by the code
    public function createRideCode()
    {
        $random_text = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet); // edited
        for ($i = 0; $i < 5; $i++) {
            $random_text .= $codeAlphabet[random_int(0, $max - 1)];
        }
        $random_text = "NSU-" . $random_text;

        return $random_text;
    }

    //function to calculate distance between two users
    public function calculateDistance($lat1, $lon1, $lat2, $lon2, $unit)
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        //distance in kilometer
        if ($unit == "K") {
            return (round(($miles * 1.609344), 2));
            //distance in nautical mile
        } else if ($unit == "N") {
            return ($miles * 0.8684);
            //distance in mile
        } else {
            return $miles;
        }
    }


    //pending request
    public function requestExist($user_id)
    {
        $exist = UserRequests::where('user_id', $user_id)->where('ride_status', 0)
            ->count();
        if ($exist > 0) {
            return true;
        } else {
            return false;
        }
    }

    //check email
    public function emailExist($email)
    {
        $exist = User::where('email', $email)
            ->count();
        if ($exist > 0) {
            return true;
        } else {
            return false;
        }
    }

    //check phone
    public function phoneExist($phone)
    {
        $exist = User::where('phone', $phone)
            ->count();
        if ($exist > 0) {
            return true;
        } else {
            return false;
        }
    }

    //check student id
    public function studentExist($student_id)
    {
        $exist = User::where('student_id', $student_id)
            ->count();
        if ($exist > 0) {
            return true;
        } else {
            return false;
        }
    }
}
