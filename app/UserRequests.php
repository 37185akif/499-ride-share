<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRequests extends Model
{
    protected $table = 'users_requests';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'start_point_id', 'destination',
        'longitude','latitude','ride_status','user_count','distance','posted_on'];
    public $timestamps = false;

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id'); // this matches the Eloquent model
    }


    public function startPoint()
    {
        return $this->hasOne('App\StartingPoints', 'id', 'start_point_id'); // this matches the Eloquent model
    }

}
