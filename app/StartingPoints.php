<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StartingPoints extends Model
{
    protected $table = 'start_points';
    protected $primaryKey = 'id';
    protected $fillable = ['place_name', 'longitude', 'latitude'];
    public $timestamps = false;
}
