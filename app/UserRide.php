<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRide extends Model
{
    protected $table = 'users_ride';
    protected $primaryKey = 'id';
    protected $fillable = ['request_id', 'ride_fare', 'share_code', 'posted_on'];
    public $timestamps = false;

    public function request()
    {
        return $this->hasOne('App\UserRequests', 'id', 'request_id'); // this matches the Eloquent model
    }
}
